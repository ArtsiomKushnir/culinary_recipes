export default {
    name: 'CategoryEditModal',
    props: {
        selectedCategory: {
            required: true,
            type: Object,
            default: { id: null, name: null, dishes: [] }
        }
    },
    data() {
        return {
            validation: {
                name: {
                    isValid: null,
                    errorMsg: null,
                    clean() {
                        this.isValid = null;
                        this.errorMsg = null;
                    },
                    validate() {
                        if (this.errorMsg) this.isValid = false;
                    }
                },
                dishes: {
                    isValid: null,
                    errorMsg: null,
                    clean() {
                        this.isValid = null;
                        this.errorMsg = null;
                    },
                    validate() {
                        if (this.errorMsg) this.isValid = false;
                    }
                },
                cleanAll() {
                    this.name.clean();
                    this.dishes.clean();
                }
            }
        }
    },
    methods: {
        update() {
            this.validation.cleanAll();

            axios
                .put(
                    '/categories',
                    this.selectedCategory)
                .then(response => {
                    bootstrap.Modal.getInstance(document.getElementById('editCategoryModal')).hide();
                    this.$emit('refresh-category-list');
                })
                .catch(error => {
                    if (error.response.status === 400) {
                        if (error.response.data.name) {
                            this.validation.name.errorMsg = error.response.data.name;
                            this.validation.name.validate();
                        }
                        if (error.response.data.dishes) {
                            this.validation.dishes.errorMsg = error.response.data.dishes;
                            this.validation.dishes.validate();
                        }
                    }
                });
        },
        deleteDish(index) {
            this.selectedCategory.dishes.splice(index,1);
        },
        addDish() {
            this.selectedCategory.dishes.push({
                name: null,
                description: null
            });
        },
        onModalClose() {
            this.validation.cleanAll();
        }
    },
    template:
        '<div class="modal fade" ' +
                'id="editCategoryModal" ' +
                'tabindex="-1" ' +
                'aria-labelledby="editNewCategoryModalLabel" ' +
                'aria-hidden="true" ' +
                'data-bs-backdrop="static" ' +
                'data-bs-keyboard="false"> ' +
            '<div class="modal-dialog modal-lg">' +
                '<div class="modal-content">' +
                    '<div class="modal-header">' +
                        '<h5 class="modal-title" id="editNewCategoryModalLabel">Редактирование категории</h5>' +
                        '<button type="button" class="btn-close" data-bs-dismiss="modal" @click="onModalClose" aria-label="Close"></button>' +
                    '</div>' +
                    '<div class="modal-body">' +
                        '<form class="needs-validation" novalidate>' +
                            '<div class="form-floating mb-3">' +
                                '<input type="text" ' +
                                        'class="form-control" ' +
                                        ':class="[ validation.name.isValid ? \'is-valid\' : validation.name.isValid === false?  \'is-invalid\' : \'\' ]" ' +
                                        'id="editCategoryName" ' +
                                        'placeholder="Наименование категории" ' +
                                        '@change="validation.name.clean()" ' +
                                        'v-model="selectedCategory.name" ' +
                                        'required aria-describedby="editCategoryNameValidationFeedback"> ' +
                                '<label for="editCategoryName">Наименование категории</label>' +
                                '<div v-if="validation.name.isValid === false" class="invalid-feedback" ' +
                                        'id="editCategoryNameValidationFeedback">' +
                                    '{{ validation.name.errorMsg }}' +
                                '</div>' +
                            '</div>' +
                            '<div class="mb-0">' +
                                '<span id="createDishesSpan">Типы блюда:</span>' +
                                '<div v-if="validation.dishes.isValid === false" style="font-size: .875em; color: #dc3545; width: 100%;" ' +
                                        'id="editDishesSpanValidationFeedback">' +
                                    '{{ validation.dishes.errorMsg }}' +
                                '</div>' +
                            '</div>' +
                            '<div class="row g-2 mb-3" v-for="(dish, index) in selectedCategory.dishes">' +
                                '<div class="col-md">' +
                                    '<input class="form-control" ' +
                                            'placeholder="Тип рецепта" ' +
                                            'v-model="dish.name">' +
                                '</div>' +
/*                                '<div class="col-1">' +
                                    '<button type="button" class="btn btn-danger btn-sm" @click="deleteDish(index)">-</button>' +
                                '</div>' +*/
                            '</div>' +
                            '<div class="row g-2 mb-3">' +
                                '<div class="col-4">' +
                                    '<button type="button" class="btn btn-success btn-sm" @click="addDish">+ Добавить тип блюда</button>' +
                                '</div>' +
                            '</div>' +
                        '</form>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-secondary" data-bs-dismiss="modal" @click="onModalClose">Закрыть</button>' +
                        '<button type="button" class="btn btn-primary" @click="update">Обновить категорию</button>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'
}