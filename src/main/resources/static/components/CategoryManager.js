import CategoryAddModal from './CategoryAddModal.vue.js'
import CategoryEditModal from './CategoryEditModal.vue.js'

export default {
    name: 'CategoryManager',
    components: {
        CategoryAddModal,
        CategoryEditModal
    },
    data() {
        return {
            categories: [],
            selectedCategory: {}
        }
    },
    mounted() {
        this.getCategories();
    },
    methods: {
        getCategories() {
            axios
                .get('/categories')
                .then(response => {
                    this.categories = response.data;
                })
                .catch(error => console.log(error));
        },
        selectCategory(cat) {
            this.selectedCategory = cat;
        }
    },

    template:
    '<main class="container mt-2">' +
        '<h1 class="mb-3">Управление категориями рецептов</h1>' +
        '<div class="row mb-3">' +
            '<button type="button" ' +
                    'class="btn align-middle btn-primary me-2" ' +
                    'data-bs-toggle="modal" ' +
                    'data-bs-target="#addNewCategoryModal">+ Добавить категорию</button> ' +
        '</div>' +
        '<table class="table table-sm table-hover">' +
            '<thead class="table-light">' +
                '<tr>' +
                    '<th scope="col">#</th>' +
                    '<th scope="col">Название</th>' +
                    '<th scope="col">Кол-во типов</th>' +
                '</tr>' +
            '</thead>' +
            '<tbody>' +
                '<tr v-for="category in categories" :key="category.id" >' +
                    '<th scope="row">{{ category.id }}</th>' +
                    '<td><a type="button" data-bs-toggle="modal" data-bs-target="#editCategoryModal" @click="selectCategory(category)">{{ category.name }}</a></td>' +
                    '<td>{{ category.dishes.length }}</td>' +
                '</tr>' +
            '</tbody>' +
        '</table>' +
        '<category-add-modal v-on:refresh-category-list="getCategories"></category-add-modal>' +
        '<category-edit-modal :selectedCategory="selectedCategory" v-on:refresh-category-list="getCategories"></category-edit-modal>' +
    '</main>'
}