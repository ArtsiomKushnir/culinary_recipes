export default {
    name: 'SearchRecipe',
    data() {
        return {
            categories: [],
            selectedCategory: null,
            dishes: [],
            selectedDish: null,
            searchName: null,

            recipes: [],
            page: {
                content: [],
                sort: null,
                pageable: {
                    sort: null,
                    offset: 0,
                    pageNumber: 0,
                    pageSize: 10,
                    paged: null,
                    unpaged: null
                },
                last: null,
                first: null,
                totalPages: null,
                totalElements: null,
                number: null,
                size: null,
                numberOfElements: null,
                empty: null
            }
        }
    },
    mounted() {
        this.getCategories();
        this.searchRecipes();
    },
    methods: {
        isSearch() {
            return this.searchName | this.selectedDish | this.selectedCategory;
        },
        getCategories() {
            axios
                .get('/categories')
                .then(response => {
                    this.categories = response.data;
                })
                .catch(error => console.log(error));
        },
        getRecipes(params) {
            axios
                .get('/recipes',
                    {
                        params: params,
                        timeout: 3000
                    }
                )
                .then(response => {
                    this.page = response.data
                    this.recipes = this.page.content;
                })
                .catch(error => console.log(error));
        },
        getDishes() {
            this.selectedDish = null;
            this.dishes = this.selectedCategory == null ? [] : this.selectedCategory.dishes
        },
        searchRecipes() {
            if (this.searchName || this.selectedCategory || this.selectedDish) {
                this.getRecipes({
                    name:       this.searchName,
                    dishId:     this.selectedDish ? this.selectedDish.id : null,
                    categoryId: this.selectedCategory ? this.selectedCategory.id : null,
                    page:       this.page.pageable.pageNumber,
                    size:       this.page.pageable.pageSize
                });
            } else {
                this.getRecipes({
                    page: this.page.pageable.pageNumber,
                    size: this.page.pageable.pageSize
                });
            }
        },
        toRecipeDetails(recipeId) {
            this.$router.push({ name: `Recipe`, params: { id: recipeId} })
        },
        previousPage() {
            this.page.pageable.pageNumber = this.page.pageable.pageNumber - 1;
            this.getRecipes({
                page: this.page.pageable.pageNumber,
                size: this.page.pageable.pageSize
            })
        },
        nextPage() {
            this.page.pageable.pageNumber = this.page.pageable.pageNumber + 1;
            this.getRecipes({
                page: this.page.pageable.pageNumber,
                size: this.page.pageable.pageSize
            })
        },
        toPage(n) {
            this.page.pageable.pageNumber = n - 1;
            this.getRecipes({
                page: this.page.pageable.pageNumber,
                size: this.page.pageable.pageSize
            })
        }
    },
    template:
    '<div>' +
        '<div class="pt-2 bg-light">' +
        '    <div class="container">' +
        '      <form>' +
        '        <div class="row g-2 mb-2">' +
        '          <div class="col-md">' +
        '            <div class="form-floating">' +
        '              <select class="form-select form-select-sm"' +
        '                      id="searchSelectedCategory"' +
        '                      aria-label="search select category"' +
        '                      v-model="selectedCategory"' +
        '                      @change="getDishes">' +
        '                <option v-for="category in categories"' +
        '                        :value="category"' +
        '                        v-bind:key="category.id">' +
        '                  {{ category.name }}' +
        '                </option>' +
        '              </select>' +
        '              <label for="searchSelectedCategory">Категория</label>' +
        '            </div>' +
        '          </div>' +
        '          <div class="col-md">' +
        '            <div class="form-floating">' +
        '              <select class="form-select form-select-sm"' +
        '                      id="searchSelectedDish"' +
        '                      aria-label="search select dish"' +
        '                      v-model="selectedDish"' +
        '                      :disabled="selectedCategory == null || selectedCategory.dishes == null || selectedCategory.dishes.length < 1">' +
        '                <option v-for="dish in dishes"' +
        '                        :value="dish"' +
        '                        v-bind:key="dish.id">' +
        '                  {{ dish.name }}' +
        '                </option>' +
        '              </select>' +
        '              <label for="searchSelectedDish">Тип блюда</label>' +
        '            </div>' +
        '          </div>' +
        '        </div>' +
        '        <div class="row mb-2">' +
        '          <div class="input-group mb-2">' +
        '            <input type="search"' +
        '                   v-model="searchName"' +
        '                   class="form-control"' +
        '                   placeholder="Поиск"' +
        '                   aria-label="searchRecipe"' +
        '                   aria-describedby="search-button">' +
        '            <button class="btn btn-outline-secondary"' +
        '                    type="button"' +
        '                    id="search-button"' +
        '                    @click="searchRecipes">Поиск</button>' +
        '          </div>' +
        '        </div>' +
        '      </form>' +
        '    </div>' +
        '</div>' +


        '<main class="container mt-2 border-bottom">' +

        '    <div class="row text-center mb-3">' +
        '       <template v-if="recipes.length > 0 && isSearch()">' +
        '           <h1>Результат поиска рецептов</h1>' +
        '       </template>' +
        '       <template v-else-if="recipes.length > 0">' +
        '           <h1>Все рецепты</h1>' +
        '       </template>' +
        '       <template v-else>' +
        '           <h1>Рецепты не найдены. Измените критерии поиска</h1>' +
        '       </template>' +
        '    </div>' +

        '    <div class="row">' +
        '      <template v-for="recipe in recipes" v-bind:key="recipe.id">' +
        '        <div class="col-sm-6" @click="toRecipeDetails(recipe.id)">' +
        '          <div class="card border-info mb-2">' +
        '            <div class="row g-0">' +
        '              <div class="col-md-5">' +
        '                <img height="128" v-bind:src=" \'/img/recipe/\' + recipe.id" v-bind:alt="recipe.name" >' +
        '              </div>' +
        '              <div class="col-md-7">' +
        '                <div class="card-body">' +
        '                  <h5 class="card-title">{{ recipe.name }}</h5>' +
        '                  <p class="card-text">' +
        '                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16">' +
        '                      <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/>' +
        '                      <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/>' +
        '                      <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>' +
        '                    </svg>' +
        '                    <small class="text-muted">{{ recipe.cookingTimeMin }} мин</small>' +
        '                  </p>' +
        '                  <span class="card-text">' +
        '                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people" viewBox="0 0 16 16">' +
        '                      <path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"/>' +
        '                    </svg>' +
        '                    <small class="text-muted">{{ recipe.numberOfServings }} порций</small>' +
        '                  </span>' +
        '                </div>' +
        '              </div>' +
        '            </div>' +
        '          </div>' +
        '        </div>' +
        '      </template>' +

        '      <nav v-if="!page.empty && (!page.last || !page.first)">' +
        '        <ul class="pagination justify-content-center pagination-sm">' +

        '          <template v-if="page.first">' +
        '            <li class="page-item disabled">' +
        '              <span class="page-link" tabindex="-1" aria-disabled="true">&laquo;</span>' +
        '            </li>' +
        '          </template>' +
        '          <template v-else>' +
        '            <li class="page-item">' +
        '              <a class="page-link" @click="previousPage" href="#">&laquo;</a>' +
        '            </li>' +
        '          </template>' +

        '          <template v-for="n in page.totalPages">' +
        '            <template v-if="n === page.number + 1">' +
        '              <li class="page-item active" aria-current="page">' +
        '                <a class="page-link" href="#">{{ n }}</a>' +
        '              </li>' +
        '            </template>' +
        '            <template v-else>' +
        '              <li class="page-item">' +
        '                <a class="page-link" @click="toPage(n)" href="#">{{ n }}</a>' +
        '              </li>' +
        '            </template>' +
        '          </template>' +

        '          <template v-if="page.last">' +
        '            <li class="page-item disabled">' +
        '              <span class="page-link">&raquo;</span>' +
        '            </li>' +
        '          </template>' +
        '          <template v-else>' +
        '            <li class="page-item">' +
        '              <a class="page-link" @click="nextPage" href="#">&raquo;</a>' +
        '            </li>' +
        '          </template>' +

        '        </ul>' +
        '      </nav>' +
        '    </div>' +
        '</main>' +

        '</div>'
}