import recipeValidateEntity from "./entity/recipeValidateEntity.js";

export default {
    name: 'AddNewRecipeModal',
    data() {
        return {
            categories: [],
            dishes: [],
            selectedCategory: null,

            recipe: {
                name: '',
                description: '',
                dishId: null,
                ingredients: [],
                steps: [],
                cookingTimeMin: 30,
                numberOfServings: 10,
            },
            file: null,

            recipeValidate: recipeValidateEntity(),
        }
    },
    mounted() {
        this.getCategories();
    },
    methods: {
        getCategories() {
            axios
                .get('/categories')
                .then(response => {
                    this.categories = response.data;
                })
                .catch(error => console.log(error));
        },
        addIngredient() {
            this.recipe.ingredients.push({
                name: null,
                count: null,
                unit: null
            })
        },
        deleteIngredient(index) {
            this.recipe.ingredients.splice(index,1);
        },
        addStep() {
            this.recipe.steps.push({
                id: this.recipe.steps.length + 1,
                description: null,
            })
        },
        deleteStep(index) {
            this.recipe.steps.splice(index,1);
        },
        getDishes() {
            this.dishes = this.selectedCategory == null ? [] : this.selectedCategory.dishes
        },
        handleFile(event) {
            this.file = event.target.files[0];
        },
        cleanAndPreValidate() {
            this.recipeValidate.cleanAll();

            if (this.recipe.dishId === null) {
                this.recipeValidate.dish.errorMsg = 'Выберите тип блюда';
                this.recipeValidate.dish.validate();
                return false;
            }
            return true;
        },

        sendCreateRequest() {
            this.recipeValidate.cleanAll();

            const formData = new FormData();

            formData.append("file", this.file);
            formData.append("name", this.recipe.name);
            formData.append("description", this.recipe.description);
            formData.append("cookingTimeMin", this.recipe.cookingTimeMin);
            formData.append("numberOfServings", this.recipe.numberOfServings);
            formData.append("dishId", this.recipe.dishId);
            formData.append('ingredients', new Blob([JSON.stringify(this.recipe.ingredients)], {
                type: "application/json"
            }));
            formData.append('steps', new Blob([JSON.stringify(this.recipe.steps)], {
                type: "application/json"
            }));

            axios
                .post(
                    '/recipes',
                    formData,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                .then(() => {
                    if (this.$router.currentRoute.name !== 'Home') {
                        this.$router.push({ name: 'Home' });
                    }
                    location.reload();
                })
                .catch(error => {
                    if (error.response.status === 400) {
                        if (error.response.data.name) {
                            this.recipeValidate.name.errorMsg = error.response.data.name;
                            this.recipeValidate.name.validate();
                        }
                        if (error.response.data.dish) {
                            this.recipeValidate.dish.errorMsg = error.response.data.dish;
                            this.recipeValidate.dish.validate();
                        }
                        if (error.response.data.cookingTimeMin) {
                            this.recipeValidate.cookingTimeMin.errorMsg = error.response.data.cookingTimeMin;
                            this.recipeValidate.cookingTimeMin.validate();
                        }
                        if (error.response.data.numberOfServings) {
                            this.recipeValidate.numberOfServings.errorMsg = error.response.data.numberOfServings;
                            this.recipeValidate.numberOfServings.validate();
                        }
                        if (error.response.data.ingredients) {
                            this.recipeValidate.ingredients.errorMsg = error.response.data.ingredients;
                            this.recipeValidate.ingredients.validate();
                        }
                        if (error.response.data.steps) {
                            this.recipeValidate.steps.errorMsg = error.response.data.steps;
                            this.recipeValidate.steps.validate();
                        }
                    }
                });
        },
        create() {
            if (this.cleanAndPreValidate()) {
                this.sendCreateRequest();
            }
        },
        onModalClose() {
            this.recipe =  {
                name: '',
                description: '',
                dishId: null,
                ingredients: [],
                steps: [],
                cookingTimeMin: 30,
                numberOfServings: 10,
            }
            this.recipeValidate.cleanAll();
        }
    },
    template: '<div class="modal fade"' +
        '       id="addNewRecipeModal"' +
        '       tabindex="-1"' +
        '       aria-labelledby="addNewRecipeModalLabel"' +
        '       aria-hidden="true"' +
        '       data-bs-backdrop="static"' +
        '       data-bs-keyboard="false">' +
        '    <div class="modal-dialog modal-lg">' +
        '      <div class="modal-content">' +
        '        <div class="modal-header">' +
        '          <h5 class="modal-title" id="addNewRecipeModalLabel">Добавить новый рецепт</h5>' +
        '          <button type="button" class="btn-close" data-bs-dismiss="modal" @click="onModalClose" aria-label="Close"></button>' +
        '        </div>' +
        '        <div class="modal-body">' +
        '          <form class="needs-validation" novalidate>' +
        '            <div class="form-floating mb-3">' +
        '              <input type="text" ' +
        '                       class="form-control" ' +
        '                       :class="[ recipeValidate.name.valid ? \'is-valid\' : recipeValidate.name.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                       id="createRecipeName" ' +
        '                       placeholder="Наименование рецепта" ' +
        '                       @change="recipeValidate.name.clean()" ' +
        '                       v-model="recipe.name"' +
        '                       required aria-describedby="createRecipeNameValidationFeedback">' +
        '              <label for="createRecipeName">Наименование рецепта</label>' +
        '              <div v-if="recipeValidate.name.valid === false" class="invalid-feedback" ' +
        '                   id="createRecipeNameValidationFeedback">' +
        '                   {{ recipeValidate.name.errorMsg }}' +
        '               </div>' +
        '            </div>' +

        '            <div class="form-floating mb-3">' +
        '              <textarea ' +
        '                   class="form-control" ' +
        '                   placeholder="Описание" ' +
        '                   id="floatingTextarea2" ' +
        '                   style="height: 150px"' +
        '                   v-model="recipe.description"></textarea>' +
        '              <label for="floatingTextarea2">Описание</label>' +
        '            </div>' +

        '            <div class="row g-2 mb-3">' +
        '              <div class="col-md">' +
        '                <div class="form-floating">' +
        '                  <select class="form-select form-select-sm"' +
        '                          id="selectedCategory"' +
        '                          v-model="selectedCategory"' +
        '                          @change="getDishes"' +
        '                          aria-label="Floating label select category">' +
        '                    <option v-for="category in categories" :value="category" v-bind:key="category.id">' +
        '                      {{ category.name }}' +
        '                    </option>' +
        '                  </select>' +
        '                  <label for="selectedCategory">Выберите категорию блюда</label>' +
        '                </div>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                <div class="form-floating">' +
        '                  <select class="form-select form-select-sm"' +
        '                          :class="[ recipeValidate.dish.valid ? \'is-valid\' : recipeValidate.dish.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                          id="selectedDish"' +
        '                          v-model="recipe.dishId"' +
        '                          @change="recipeValidate.dish.clean()" ' +
        '                          :disabled="selectedCategory == null"' +
        '                          required aria-describedby="createRecipeDishValidationFeedback">' +
        '                    <option v-for="dish in dishes" :value="dish.id" v-bind:key="dish.id">' +
        '                      {{ dish.name }}' +
        '                    </option>' +
        '                  </select>' +
        '                  <label for="selectedDish">Выберите тип блюда</label>' +
        '                  <div v-if="recipeValidate.dish.valid == false" class="invalid-feedback" ' +
        '                       id="createRecipeDishValidationFeedback">' +
        '                       {{ recipeValidate.dish.errorMsg }}' +
        '                  </div>' +
        '                </div>' +
        '              </div>' +
        '            </div>' +

        '            <div class="mb-3">' +
        '              <label for="inputGroupFile01">Файл изображения</label>' +
        '              <input   type="file" ' +
        '                       class="form-control" ' +
        '                       id="inputGroupFile01"' +
        '                       @change="handleFile"' +
        '                       placeholder="Файл изображения">' +
        '            </div>' +

        '            <div class="mb-0">' +
        '                   <span id="createRecipeIngredientsSpan">Список ингредиентов:</span>' +
        '                   <div v-if="recipeValidate.ingredients.valid === false" style="font-size: .875em; color: #dc3545; width: 100%;" ' +
        '                           id="createRecipeIngredientsValidationFeedback">' +
        '                           {{ recipeValidate.ingredients.errorMsg }}' +
        '                   </div>' +
        '            </div>' +
        '            <div class="row g-2 mb-3" v-for="(ingredient, index) in recipe.ingredients">' +
        '              <div class="col-md">' +
        '                   <input v-model="ingredient.name" class="form-control form-control-sm" type="text" placeholder="Наименование">' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <input v-model="ingredient.count" class="form-control form-control-sm" type="number" placeholder="количество">' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <input v-model="ingredient.unit" class="form-control form-control-sm" type="text" placeholder="Ед. измерения">' +
        '              </div>' +
        '              <div class="col-1">' +
        '                   <button type="button" class="btn btn-danger btn-sm" @click="deleteIngredient(index)">-</button>' +
        '              </div>' +
        '            </div>' +
        '            <div class="row g-2 mb-3">' +
        '              <div class="col-4">' +
        '                   <button type="button" class="btn btn-success btn-sm" @click="addIngredient">+ Добавить ингредиент</button>' +
        '              </div>' +
        '            </div>' +

        '            <div class="mb-0">' +
        '                   <span id="createRecipeStepsSpan">Инструкции приготовления:</span>' +
        '                   <div v-if="recipeValidate.steps.valid === false" style="font-size: .875em; color: #dc3545; width: 100%;" ' +
        '                           id="createRecipeStepsValidationFeedback">' +
        '                           {{ recipeValidate.steps.errorMsg }}' +
        '                   </div>' +
        '            </div>' +
        '            <div class="row g-2 mb-3" v-for="(step, index) in recipe.steps">' +
        '              <div class="col-1">' +
        '                   <span>{{ step.id }}</span>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <textarea ' +
        '                           class="form-control" ' +
        '                           placeholder="Шаг инструкции" ' +
        '                           style="height: 50px"' +
        '                           v-model="step.description">' +
        '                   </textarea>' +
        '                   <input v-model="step.id" type="hidden" >' +
        '              </div>' +
        '              <div class="col-1">' +
        '                   <button type="button" class="btn btn-danger btn-sm" @click="deleteStep(index)">-</button>' +
        '              </div>' +
        '            </div>' +
        '            <div class="row g-2 mb-3">' +
        '              <div class="col-4">' +
        '                   <button type="button" class="btn btn-success btn-sm" @click="addStep">+ Добавить шаг</button>' +
        '              </div>' +
        '            </div>' +

        '            <div class="row g-2">' +
        '              <div class="col-md">' +
        '                <div class="form-floating">' +
        '                  <input type="number" ' +
        '                           class="form-control" ' +
        '                           :class="[ recipeValidate.cookingTimeMin.valid ? \'is-valid\' : recipeValidate.cookingTimeMin.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                           id="createRecipeCookingTimeMin" ' +
        '                           @change="recipeValidate.cookingTimeMin.clean()" ' +
        '                           placeholder="Время приготовления (минут)" ' +
        '                           v-model="recipe.cookingTimeMin"' +
        '                           required aria-describedby="createRecipeCookingTimeMinValidationFeedback">' +
        '                  <label for="createRecipeCookingTimeMin">Время приготовления (минут)</label>' +
        '                  <div v-show="recipeValidate.cookingTimeMin.valid === false" class="invalid-feedback" ' +
        '                       id="createRecipeCookingTimeMinValidationFeedback">' +
        '                       {{ recipeValidate.cookingTimeMin.errorMsg }}' +
        '                  </div>' +
        '                </div>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                <div class="form-floating">' +
        '                  <input type="number" ' +
        '                           class="form-control" ' +
        '                           :class="[ recipeValidate.numberOfServings.valid ? \'is-valid\' : recipeValidate.numberOfServings.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                           id="createRecipeNumberOfServings" ' +
        '                           @change="recipeValidate.numberOfServings.clean()" ' +
        '                           placeholder="Количество порций (шт.)" ' +
        '                           v-model="recipe.numberOfServings"' +
        '                           required aria-describedby="createRecipeNumberOfServingsValidationFeedback">' +
        '                  <label for="createRecipeNumberOfServings">Количество порций (шт.)</label>' +
        '                  <div v-show="recipeValidate.numberOfServings.valid === false" class="invalid-feedback" ' +
        '                       id="createRecipeNumberOfServingsValidationFeedback">' +
        '                       {{ recipeValidate.numberOfServings.errorMsg }}' +
        '                  </div>' +
        '                </div>' +
        '              </div>' +
        '            </div>' +
        '          </form>' +
        '        </div>' +
        '        <div class="modal-footer">' +
        '          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" @click="onModalClose">Закрыть</button>' +
        '          <button type="button" class="btn btn-primary" @click="create">Создать рецепт</button>' +
        '        </div>' +
        '      </div>' +
        '    </div>' +
        '  </div>'
}