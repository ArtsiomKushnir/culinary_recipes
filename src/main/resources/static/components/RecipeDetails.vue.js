import recipeValidateEntity from './entity/recipeValidateEntity.js'

export default {
    name: 'RecipeDetails',
    props: ['id'],
    data() {
        return {
            dishes: [],
            originalRecipe: {},
            recipe: {
                dish: {}
            },
            file: null,
            recipeValidate: recipeValidateEntity(),

            disabled: true,
        }
    },
    mounted() {
        this.getRecipe();
        this.getDishes();
    },
    methods: {
        allowEdit() {
            this.disabled = false;
        },
        getRecipe() {
            axios
                .get('/recipes/' + this.id)
                .then(response => {
                    this.recipe = response.data;
                    this.originalRecipe = Object.assign(this.originalRecipe, this.recipe);
                })
                .catch(error => console.log(error));
        },
        saveRecipe() {

            this.recipeValidate.cleanAll();

            const formData = new FormData();

            formData.append("file", this.file);
            formData.append("id", this.recipe.id);
            formData.append("name", this.recipe.name);
            formData.append("description", this.recipe.description);
            formData.append("cookingTimeMin", this.recipe.cookingTimeMin);
            formData.append("numberOfServings", this.recipe.numberOfServings);
            formData.append("dishId" ,this.recipe.dish.id);
            formData.append('ingredients', new Blob([JSON.stringify(this.recipe.ingredients)], {
                type: "application/json"
            }));
            formData.append('steps', new Blob([JSON.stringify(this.recipe.steps)], {
                type: "application/json"
            }));

            axios
                .put(
                    '/recipes',
                    formData,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                .then(response => {
                    this.disabled = true;
                    this.recipe = response.data;
                    this.originalRecipe = Object.assign(this.originalRecipe, this.recipe);
                })
                .catch(error => {
                    if (error.response.status === 400) {
                        if (error.response.data.name) {
                            this.recipeValidate.name.errorMsg = error.response.data.name;
                            this.recipeValidate.name.validate();
                        }
                        if (error.response.data.dish) {
                            this.recipeValidate.dish.errorMsg = error.response.data.dish;
                            this.recipeValidate.dish.validate();
                        }
                        if (error.response.data.cookingTimeMin) {
                            this.recipeValidate.cookingTimeMin.errorMsg = error.response.data.cookingTimeMin;
                            this.recipeValidate.cookingTimeMin.validate();
                        }
                        if (error.response.data.numberOfServings) {
                            this.recipeValidate.numberOfServings.errorMsg = error.response.data.numberOfServings;
                            this.recipeValidate.numberOfServings.validate();
                        }
                        if (error.response.data.ingredients) {
                            this.recipeValidate.ingredients.errorMsg = error.response.data.ingredients;
                            this.recipeValidate.ingredients.validate();
                        }
                        if (error.response.data.steps) {
                            this.recipeValidate.steps.errorMsg = error.response.data.steps;
                            this.recipeValidate.steps.validate();
                        }
                    }
                });
        },
        deleteRecipe() {
            axios
                .delete('/recipes/' + this.recipe.id)
                .then(() => this.$router.push({ name: 'Home' }))
                .catch(error => console.log(error));
        },
        cancelEdit() {
            this.disabled = true;
            this.recipe = Object.assign(this.recipe, this.originalRecipe);
        },
        getDishes() {
            axios
                .get('/dishes')
                .then(response => this.dishes = response.data)
                .catch(error => console.log(error));
        },
        handleFile(event) {
            this.file = event.target.files[0];
        },
        deleteIngredient(index) {
            this.recipe.ingredients.splice(index,1);
        },
        addIngredient() {
            this.recipe.ingredients.push({
                name: null,
                count: null,
                unit: null
            })
        },
        deleteStep(index) {
            this.recipe.steps.splice(index,1);
        },
        addStep() {
            this.recipe.steps.push({
                id: this.recipe.steps.length + 1,
                description: null,
            })
        }
    },
    template:
        '<div>' +

        '   <div class="pt-2 bg-light border-bottom">' +
        '       <div class="container mb-2">' +
        '           <button type="button" v-show="disabled" class="btn align-middle btn-info" @click="allowEdit">Редактировать рецепт</button>' +
        '           <button type="button" v-show="!disabled" class="btn align-middle btn-success" @click="saveRecipe">Сохранить</button>' +
        '           <button type="button" v-show="!disabled" class="btn align-middle btn-warning" @click="cancelEdit">Отмена</button>' +
        '           <button type="button" v-show="!disabled" class="btn align-middle btn-danger" @click="deleteRecipe">Удалить рецепт</button>' +
        '       </div>' +
        '   </div>' +

        '   <main class="container mt-2">' +

        '   <template v-if="disabled">' +
        '       <div class="px-4 pt-5 my-5 mb-5 text-center border-bottom">' +
        '           <h1 class="display-4 fw-bold">{{ recipe.name }}</h1>' +
        '           <div class="col-lg-6 mx-auto">' +
        '               <p class="lead mb-4">{{ recipe.description }}</p>' +
        '               <div class="d-grid gap-2 d-sm-flex justify-content-sm-center mb-5">' +

        '                  <span class="card-text me-5">' +
        '                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16">' +
        '                      <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/>' +
        '                      <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/>' +
        '                      <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>' +
        '                    </svg>' +
        '                    <small class="text-muted">{{ recipe.cookingTimeMin }} мин</small>' +
        '                  </span>' +

        '                  <span class="card-text">' +
        '                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-people" viewBox="0 0 16 16">' +
        '                      <path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"/>' +
        '                    </svg>' +
        '                    <small class="text-muted">{{ recipe.numberOfServings }} {{ recipe.numberOfServings === 1 ? \'порция\' : recipe.numberOfServings > 1 && recipe.numberOfServings < 5 ? \'порции\' : \'порций\' }}</small>' +
        '                  </span>' +

        '               </div>' +
        '           </div>' +
        '           <div class="overflow-hidden mb-5" style="max-height: 50vh;">' +
        '               <div class="container px-5">' +
        '                   <img v-bind:src=" \'/img/recipe/\' + id" class="img-fluid border rounded-3 shadow-lg mb-4" ' +
        '                           :alt="recipe.name" ' +
        '                           width="570" ' +
        '                           height="570" ' +
        '                           loading="lazy">' +
        '               </div>' +
        '           </div>' +

                    '<div class="col-md">' +
                        '<p class="mb-3"><strong>Ингредиенты</strong></p>' +
                        '<table class="table table-borderless">' +
                            '<tbody>' +
                                '<tr v-for="ingredient in recipe.ingredients" :key="ingredient.name">' +
                                    '<td>{{ ingredient.name }}</td>' +
                                    '<td>{{ ingredient.count + \' \' + ingredient.unit }}</td>' +
                                '</tr>' +
                            '</tbody>' +
                        '</table>' +
                    '</div>' +

                    '<div class="col-md">' +
                        '<p class="mb-3"><strong>Инструкция приготовления</strong></p>' +
                        '<table class="table table-borderless">' +
                            '<tbody>' +
                                '<tr v-for="step in recipe.steps" :key="step.id">' +
                                    '<td>{{ step.id }}</td>' +
                                    '<td>{{ step.description }}</td>' +
                                '</tr>' +
                            '</tbody>' +
                        '</table>' +
                    '</div>' +

        '       </div>' +
        '   </template>' +


        '   <template v-else>' +
        '          <form class="needs-validation mb-5" novalidate>' +
        '            <div class="form-floating mb-3">' +
        '              <input type="text" ' +
        '                   class="form-control"' +
        '                   :class="[ recipeValidate.name.valid ? \'is-valid\' : recipeValidate.name.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                   id="editRecipeName" ' +
        '                   placeholder="Наименование рецепта"' +
        '                   v-model="recipe.name"' +
        '                   @change="recipeValidate.name.clean()"' +
        '                   required aria-describedby="editRecipeName editRecipeNameValidationFeedback">' +
        '              <label for="editRecipeName">Наименование рецепта</label>' +
        '              <div v-show="recipeValidate.name.valid === false" class="invalid-feedback" ' +
        '                   id="editRecipeNameValidationFeedback">' +
        '                   {{ recipeValidate.name.errorMsg }}' +
        '               </div>' +
        '            </div>' +
        '            <div class="form-floating mb-3">' +
        '              <textarea ' +
        '                   class="form-control" ' +
        '                   placeholder="Описание блюда" ' +
        '                   id="editRecipeDescription" ' +
        '                   style="height: 100px"' +
        '                   v-model="recipe.description"></textarea>' +
        '              <label for="editRecipeDescription">Описание блюда</label>' +
        '            </div>' +
        '            <div class="row g-2 mb-3">' +
        '              <div class="col-md">' +
        '                <div class="form-floating">' +
        '                  <select class="form-select form-select-sm"' +
        '                          :class="[ recipeValidate.dish.valid ? \'is-valid\' : recipeValidate.dish.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                          id="editRecipeDish"' +
        '                          v-model="recipe.dish" ' +
        '                          @change="recipeValidate.dish.clean()"' +
        '                          required aria-describedby="editRecipeDish editRecipeDishValidationFeedback">' +
        '                    <option v-for="dish in dishes" :selected="dish.id === recipe.dish.id" :value="dish" v-bind:key="dish.id">' +
        '                      {{ dish.name }}' +
        '                    </option>' +
        '                  </select>' +
        '                  <label for="editRecipeDish">Выберите тип блюда</label>' +
        '                  <div v-show="recipeValidate.dish.valid === false" class="invalid-feedback" ' +
        '                       id="editRecipeDishValidationFeedback">' +
        '                       {{ recipeValidate.dish.errorMsg }}' +
        '                   </div>' +
        '                </div>' +
        '              </div>' +
        '            </div>' +
        '            <div class="mb-3">' +
        '              <label for="editRecipeImageFile">Файл нового изображения (опционально)</label>' +
        '              <input   type="file" ' +
        '                       class="form-control" ' +
        '                       id="editRecipeImageFile"' +
        '                       @change="handleFile"' +
        '                       placeholder="Файл изображения">' +
        '            </div>' +

        '            <div class="mb-0">' +
        '                   <span id="editRecipeIngredientsSpan">Список ингредиентов:</span>' +
        '                   <div v-if="recipeValidate.ingredients.valid === false" style="font-size: .875em; color: #dc3545; width: 100%;" ' +
        '                           id="editRecipeIngredientsValidationFeedback">' +
        '                           {{ recipeValidate.ingredients.errorMsg }}' +
        '                   </div>' +
        '            </div>' +
        '            <div class="row g-2 mb-3" v-for="(ingredient, index) in recipe.ingredients">' +
        '              <div class="col-md">' +
        '                   <input v-model="ingredient.name" class="form-control form-control-sm" type="text" placeholder="Наименование" required>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <input v-model="ingredient.count" class="form-control form-control-sm" type="number" placeholder="Количество" required>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <input v-model="ingredient.unit" class="form-control form-control-sm" type="text" placeholder="Ед. измерения" required>' +
        '              </div>' +
        '              <div class="col-1">' +
        '                   <button type="button" class="btn btn-danger btn-sm" @click="deleteIngredient(index)">-</button>' +
        '              </div>' +
        '            </div>' +
        '            <div class="row g-2 mb-3">' +
        '              <div class="col-4">' +
        '                   <button type="button" class="btn btn-success btn-sm" @click="addIngredient">+ Добавить ингредиент</button>' +
        '              </div>' +
        '            </div>' +

        '            <div class="mb-0">' +
        '                   <span id="editRecipeStepsSpan">Инструкции приготовления:</span>' +
        '                   <div v-if="recipeValidate.steps.valid === false" style="font-size: .875em; color: #dc3545; width: 100%;" ' +
        '                           id="editRecipeStepsValidationFeedback">' +
        '                           {{ recipeValidate.steps.errorMsg }}' +
        '                   </div>' +
        '            </div>' +
        '            <div class="row g-2 mb-3" v-for="(step, index) in recipe.steps">' +
        '              <div class="col-1">' +
        '                   <span>{{ step.id }}</span>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <textarea ' +
        '                           class="form-control" ' +
        '                           placeholder="Шаг инструкции" ' +
        '                           style="height: 50px"' +
        '                           v-model="step.description">' +
        '                   </textarea>' +
        '                   <input v-model="step.id" type="hidden" >' +
        '              </div>' +
        '              <div class="col-1">' +
        '                   <button type="button" class="btn btn-danger btn-sm" @click="deleteStep(index)">-</button>' +
        '              </div>' +
        '            </div>' +
        '            <div class="row g-2 mb-3">' +
        '              <div class="col-4">' +
        '                   <button type="button" class="btn btn-success btn-sm" @click="addStep">+ Добавить шаг</button>' +
        '              </div>' +
        '            </div>' +

        '            <div class="row g-2">' +
        '              <div class="col-md">' +
        '                <div class="form-floating">' +
        '                  <input type="number" class="form-control" ' +
        '                       :class="[ recipeValidate.cookingTimeMin.valid ? \'is-valid\' : recipeValidate.cookingTimeMin.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                       id="editRecipeCookingTimeMin" ' +
        '                       @change="recipeValidate.cookingTimeMin.clean()"' +
        '                       placeholder="Время приготовления (минут)" ' +
        '                       v-model="recipe.cookingTimeMin" ' +
        '                       required aria-describedby="editRecipeCookingTimeMin editRecipeCookingTimeMinValidationFeedback">' +
        '                  <label for="editRecipeCookingTimeMin">Время приготовления (минут)</label>' +
        '                  <div v-show="recipeValidate.cookingTimeMin.valid === false" class="invalid-feedback" ' +
        '                       id="editRecipeCookingTimeMinValidationFeedback">' +
        '                       {{ recipeValidate.cookingTimeMin.errorMsg }}' +
        '                  </div>' +
        '                </div>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                <div class="form-floating">' +
        '                  <input type="number" class="form-control" ' +
        '                       :class="[ recipeValidate.numberOfServings.valid ? \'is-valid\' : recipeValidate.numberOfServings.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                       id="editRecipeNumberOfServings" ' +
        '                       @change="recipeValidate.numberOfServings.clean()"' +
        '                       placeholder="Количество порций (шт.)" ' +
        '                       v-model="recipe.numberOfServings" ' +
        '                       required aria-describedby="editRecipeNumberOfServings editRecipeNumberOfServingsValidationFeedback">' +
        '                  <label for="editRecipeNumberOfServings">Количество порций (шт.)</label>' +
        '                  <div v-show="recipeValidate.numberOfServings.valid === false" class="invalid-feedback" ' +
        '                       id="editRecipeNumberOfServingsValidationFeedback">' +
        '                       {{ recipeValidate.numberOfServings.errorMsg }}' +
        '                  </div>' +
        '                </div>' +
        '              </div>' +
        '            </div>' +
        '          </form>' +
        '   </template>' +
        '   </main>' +
        '</div>'
}