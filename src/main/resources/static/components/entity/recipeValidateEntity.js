const recipeValidateEntity = function() {
    return {
        name: {
            errorMsg: null,
            valid: null,
            clean() {
                this.valid = null;
                this.errorMsg = null;
            },
            validate() {
                if (this.errorMsg) this.valid = false;
            }
        },
        dish: {
            errorMsg: null,
            valid: null,
            clean() {
                this.valid = null;
                this.errorMsg = null;
            },
            validate() {
                if (this.errorMsg) this.valid = false;
            }
        },
        cookingTimeMin: {
            errorMsg: null,
            valid: null,
            clean() {
                this.valid = null;
                this.errorMsg = null;
            },
            validate() {
                if (this.errorMsg) this.valid = false;
            }
        },
        numberOfServings: {
            errorMsg: null,
            valid: null,
            clean() {
                this.valid = null;
                this.errorMsg = null;
            },
            validate() {
                if (this.errorMsg) this.valid = false;
            }
        },
        ingredients: {
            errorMsg: null,
            valid: null,
            clean() {
                this.valid = null;
                this.errorMsg = null;
            },
            validate() {
                if (this.errorMsg) this.valid = false;
            }
        },
        steps: {
            errorMsg: null,
            valid: null,
            clean() {
                this.valid = null;
                this.errorMsg = null;
            },
            validate() {
                if (this.errorMsg) this.valid = false;
            }
        },

        cleanAll() {
            this.name.clean();
            this.dish.clean();
            this.cookingTimeMin.clean();
            this.numberOfServings.clean();
            this.ingredients.clean();
        }
    };
};

export default recipeValidateEntity;