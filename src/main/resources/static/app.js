import SearchRecipe from './components/SearchRecipe.vue.js'
import RecipeDetails from './components/RecipeDetails.vue.js'
import AddNewRecipeModal from './components/AddNewRecipeModal.vue.js';
import CategoryManager from './components/CategoryManager.js';

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: SearchRecipe
        },
        {
            path: '/categories',
            name: 'Category',
            component: CategoryManager
        },
        {
            path: '/:id',
            name: 'Recipe',
            component: RecipeDetails,
            props: true
        }
    ],
    mode: 'history',
    base: '/'
});


const app = new Vue({
    el: '#app',
    router: router,
    methods: {
        toCategoryManager() {
            this.$router.push({ name: 'Category' });
        }
    },
    components: {
        AddNewRecipeModal
    }
});