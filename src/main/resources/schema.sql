CREATE TABLE IF NOT EXISTS category (
    id                  INT                 AUTO_INCREMENT PRIMARY KEY,
    name                VARCHAR(150)        NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS dish (
    id                  INT                 AUTO_INCREMENT PRIMARY KEY,
    name                VARCHAR(150)        NOT NULL,
    category_id         INT                 NOT NULL,
    UNIQUE (name, category_id),
    FOREIGN KEY (category_id) REFERENCES category(id)
);

CREATE TABLE IF NOT EXISTS recipe (
    id                  BIGINT              AUTO_INCREMENT PRIMARY KEY,
    name                VARCHAR(150)        NOT NULL,
    description         TEXT                ,
    cooking_time_min    SMALLINT            NOT NULL,
    number_of_servings  TINYINT             NOT NULL DEFAULT 1,
    dish_id             BIGINT              NOT NULL,
    FOREIGN KEY (dish_id) REFERENCES dish(id)
);

CREATE TABLE IF NOT EXISTS recipe_ingredients (
    recipe_id           BIGINT              NOT NULL,
    name                VARCHAR(150)        NOT NULL,
    count               DECIMAL             NOT NULL,
    unit                VARCHAR(150)        NOT NULL,
    PRIMARY KEY (recipe_id, name),
    FOREIGN KEY (recipe_id) REFERENCES recipe(id)
);

CREATE TABLE IF NOT EXISTS recipe_steps (
    recipe_id           BIGINT              NOT NULL,
    step_id             BIGINT              NOT NULL,
    step_description    TEXT                NOT NULL,
    PRIMARY KEY (recipe_id, step_id),
    FOREIGN KEY (recipe_id) REFERENCES recipe(id)
);
