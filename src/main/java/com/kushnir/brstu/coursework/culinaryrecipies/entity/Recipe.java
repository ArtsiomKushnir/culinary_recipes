package com.kushnir.brstu.coursework.culinaryrecipies.entity;

import com.kushnir.brstu.coursework.culinaryrecipies.entity.embedded.Ingredient;
import com.kushnir.brstu.coursework.culinaryrecipies.entity.embedded.Step;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "recipe")
public class Recipe {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private String name;
    private String description;
    private short cookingTimeMin;
    private short numberOfServings;

    @ManyToOne
    private Dish dish;

    @ElementCollection
    @CollectionTable(name = "recipe_ingredients", joinColumns = @JoinColumn(name = "recipe_id"))
    private List<Ingredient> ingredients;

    @ElementCollection
    @CollectionTable(name = "recipe_steps", joinColumns = @JoinColumn(name = "recipe_id"))
    private List<Step> steps;
}
