package com.kushnir.brstu.coursework.culinaryrecipies.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;
    private String name;

    /*@ElementCollection
    @CollectionTable(name = "dish", joinColumns = @JoinColumn(name = "category_id"))*/
    @OneToMany(mappedBy = "category", cascade = ALL)
    @JsonManagedReference
    private List<Dish> dishes;
}
