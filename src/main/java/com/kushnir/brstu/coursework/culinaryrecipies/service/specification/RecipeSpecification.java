package com.kushnir.brstu.coursework.culinaryrecipies.service.specification;

import com.kushnir.brstu.coursework.culinaryrecipies.entity.Recipe;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.List;

import static com.kushnir.brstu.coursework.culinaryrecipies.service.specification.RecipeSpecificationBuilder.aPredicates;

public class RecipeSpecification {

    public static Specification<Recipe> buildSpecification(final RecipeSearchCriteria recipeSearchCriteria) {
        return (root, query, cb) -> {

            if(recipeSearchCriteria == null) {
                return null;
            }

            final List<Predicate> predicates = aPredicates(root, cb, query)
                    .withName(recipeSearchCriteria.getName())
                    .withDishId(recipeSearchCriteria.getDishId())
                    .withCategoryId(recipeSearchCriteria.getCategoryId())
                    .build();

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
