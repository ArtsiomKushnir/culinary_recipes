package com.kushnir.brstu.coursework.culinaryrecipies.exception;

import java.util.Map;

public interface BaseException {
    Map<String, String> getErrors();
}
