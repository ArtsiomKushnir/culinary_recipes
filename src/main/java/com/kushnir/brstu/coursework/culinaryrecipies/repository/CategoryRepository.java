package com.kushnir.brstu.coursework.culinaryrecipies.repository;

import com.kushnir.brstu.coursework.culinaryrecipies.entity.Category;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CategoryRepository extends PagingAndSortingRepository<Category, Integer> {
    List<Category> findAll();
    boolean existsByName(final String name);
}