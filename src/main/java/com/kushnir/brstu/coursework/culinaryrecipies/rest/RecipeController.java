package com.kushnir.brstu.coursework.culinaryrecipies.rest;

import com.kushnir.brstu.coursework.culinaryrecipies.dto.RecipeDto;
import com.kushnir.brstu.coursework.culinaryrecipies.entity.Recipe;
import com.kushnir.brstu.coursework.culinaryrecipies.entity.embedded.Ingredient;
import com.kushnir.brstu.coursework.culinaryrecipies.entity.embedded.Step;
import com.kushnir.brstu.coursework.culinaryrecipies.exception.InvalidRecipeDtoRequestException;
import com.kushnir.brstu.coursework.culinaryrecipies.service.RecipeService;
import com.kushnir.brstu.coursework.culinaryrecipies.service.specification.RecipeSearchCriteria;
import com.kushnir.brstu.coursework.culinaryrecipies.service.validator.RecipeDtoValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static org.springframework.data.domain.Sort.Direction.ASC;

@RestController
@RequestMapping("/recipes")
@RequiredArgsConstructor
public class RecipeController {
    private final RecipeService recipeService;
    private final RecipeDtoValidator recipeDtoValidator;

    @GetMapping
    public Page<Recipe> findAll(
            @RequestParam(required = false) final Integer categoryId,
            @RequestParam(required = false) final Integer dishId,
            @RequestParam(required = false) final String name,
            @SortDefault(sort = "name", direction = ASC) final Pageable pageable) {

        final RecipeSearchCriteria recipeSearchCriteria = RecipeSearchCriteria.builder()
                .name(name)
                .dishId(dishId)
                .categoryId(categoryId)
                .build();

        return recipeService.findAll(pageable, recipeSearchCriteria);
    }

    @GetMapping("/{id}")
    public Recipe findById(@PathVariable final Long id) {
        return recipeService.findById(id).orElseThrow();
    }

    @PostMapping
    public Recipe create(@RequestParam(required = false) final MultipartFile file,
                         final RecipeDto recipeDto,
                         @RequestPart final List<Ingredient> ingredients,
                         @RequestPart final List<Step> steps) throws IOException, InvalidRecipeDtoRequestException {
        recipeDtoValidator.validate(recipeDto, ingredients, steps);
        return recipeService.create(recipeDto, ingredients, steps, file);
    }

    @PutMapping
    public Recipe updateRecipe(@RequestParam(required = false) final MultipartFile file,
                               final RecipeDto recipeDto,
                               @RequestPart final List<Ingredient> ingredients,
                               @RequestPart final List<Step> steps) throws InvalidRecipeDtoRequestException, IOException {
        recipeDtoValidator.validate(recipeDto, ingredients, steps);
        return recipeService.updateRecipe(recipeDto, ingredients, steps, file);
    }

    @DeleteMapping("/{id}")
    public void deleteRecipe(@PathVariable final Long id) {
        recipeService.deleteRecipe(id);
    }

}
