package com.kushnir.brstu.coursework.culinaryrecipies.service;

import com.kushnir.brstu.coursework.culinaryrecipies.dto.RecipeDto;
import com.kushnir.brstu.coursework.culinaryrecipies.entity.Recipe;
import com.kushnir.brstu.coursework.culinaryrecipies.entity.embedded.Ingredient;
import com.kushnir.brstu.coursework.culinaryrecipies.entity.embedded.Step;
import com.kushnir.brstu.coursework.culinaryrecipies.repository.DishRepository;
import com.kushnir.brstu.coursework.culinaryrecipies.repository.RecipeRepository;
import com.kushnir.brstu.coursework.culinaryrecipies.service.specification.RecipeSearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static com.kushnir.brstu.coursework.culinaryrecipies.service.specification.RecipeSpecification.buildSpecification;

@Service
@RequiredArgsConstructor
public class RecipeService {

    private final RecipeRepository recipeRepository;
    private final DishRepository dishRepository;

    public Page<Recipe> findAll(final Pageable pageable, final RecipeSearchCriteria recipeSearchCriteria) {
        final Specification<Recipe> specification = buildSpecification(recipeSearchCriteria);
        return recipeRepository.findAll(specification, pageable);
    }

    public Optional<Recipe> findById(final Long id) {
        return recipeRepository.findById(id);
    }

    public Recipe create(final RecipeDto recipeDto, final List<Ingredient> ingredients, final List<Step> steps, final MultipartFile file) throws IOException {

        final Recipe recipe = Recipe.builder()
                .name(recipeDto.getName())
                .description(recipeDto.getDescription())
                .cookingTimeMin(recipeDto.getCookingTimeMin())
                .numberOfServings(recipeDto.getNumberOfServings())
                .dish(dishRepository.findById(recipeDto.getDishId()).get())
                .ingredients(ingredients)
                .steps(steps)
                .build();

        final Recipe createdRecipe = recipeRepository.save(recipe);

        saveFIle(file, createdRecipe.getId());

        return createdRecipe;
    }

    public void deleteRecipe(final Long id) {
        recipeRepository.deleteById(id);
    }

    public Recipe updateRecipe(final RecipeDto recipeDto, final List<Ingredient> ingredients, final List<Step> steps, final MultipartFile file) throws IOException {

        final Recipe recipe = Recipe.builder()
                .id(recipeDto.getId())
                .name(recipeDto.getName())
                .description(recipeDto.getDescription())
                .cookingTimeMin(recipeDto.getCookingTimeMin())
                .numberOfServings(recipeDto.getNumberOfServings())
                .dish(dishRepository.findById(recipeDto.getDishId()).get())
                .ingredients(ingredients)
                .steps(steps)
                .build();

        // TODO update file
        saveFIle(file, recipe.getId());

        return recipeRepository.save(recipe);
    }

    private void saveFIle(final MultipartFile file, final Long id) throws IOException {
        if (file != null) {
            File convertFile = new File("target/classes/static/img/recipe/" + id);
            if (convertFile.exists()) {
                convertFile.delete();
            }

            convertFile.createNewFile();
            FileOutputStream fout = new FileOutputStream(convertFile);
            fout.write(file.getBytes());
            fout.close();
        }
    }
}
