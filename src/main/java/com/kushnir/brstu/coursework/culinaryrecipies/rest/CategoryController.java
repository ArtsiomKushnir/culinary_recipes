package com.kushnir.brstu.coursework.culinaryrecipies.rest;

import com.kushnir.brstu.coursework.culinaryrecipies.entity.Category;
import com.kushnir.brstu.coursework.culinaryrecipies.exception.InvalidCategoryRequestException;
import com.kushnir.brstu.coursework.culinaryrecipies.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping
    public List<Category> getAllCategories() {
        return categoryService.getAllCategories();
    }

    @GetMapping("/{id}")
    public Category getById(@PathVariable final Integer id) {
        return categoryService.getById(id).orElseThrow();
    }

    @PostMapping
    public Category create(@RequestBody final Category category) throws InvalidCategoryRequestException {
        return categoryService.create(category);
    }

    @PutMapping
    public Category update(@RequestBody final Category category) throws InvalidCategoryRequestException {
        return categoryService.update(category);
    }
}
