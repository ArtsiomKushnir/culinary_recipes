package com.kushnir.brstu.coursework.culinaryrecipies.service.validator;

import com.kushnir.brstu.coursework.culinaryrecipies.dto.RecipeDto;
import com.kushnir.brstu.coursework.culinaryrecipies.entity.embedded.Ingredient;
import com.kushnir.brstu.coursework.culinaryrecipies.entity.embedded.Step;
import com.kushnir.brstu.coursework.culinaryrecipies.exception.InvalidRecipeDtoRequestException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;
import static org.apache.logging.log4j.util.Strings.isBlank;
import static org.hibernate.internal.util.collections.CollectionHelper.isEmpty;
import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;

@Component
@RequiredArgsConstructor
public class RecipeDtoValidator {
    public void validate(final RecipeDto recipeDto, final List<Ingredient> ingredients, final List<Step> steps) throws InvalidRecipeDtoRequestException {
        final Map<String, String> errors = new HashMap<>();

        if (isBlank(recipeDto.getName())) {
            errors.put("name", "Введите название рецепта");
        } else if (recipeDto.getName().length() > 150) {
            errors.put("name", "Превышено ограничение в 150 символов");
        }

        if (recipeDto.getCookingTimeMin() < 1) {
            errors.put("cokingTimeMin", "Слишком маленькое значение времени приготовления блюда");
        }

        if (recipeDto.getNumberOfServings() < 1) {
            errors.put("numberOfServings", "Расчетное количество порций не может быть меньше 1");
        }

        if (isEmpty(ingredients)) {
            errors.put("ingredients", "Добавьте хотя бы один ингредиент");
        } else {
            ingredients.forEach(i -> {
                if (isBlank(i.getName())) {
                    errors.put("ingredients", "Укажите название ингредиента");
                } else if (i.getCount() == null || i.getCount().doubleValue() < 0.001) {
                    errors.put("ingredients", format("Укажите количество для: \"%s\"", i.getName()));
                } else if(isBlank(i.getUnit())) {
                    errors.put("ingredients", format("Укажите единицу измерения для : \"%s\"", i.getName()));
                }
            });
        }

        if (isEmpty(steps)) {
            errors.put("steps", "Добавьте хотя бы один шаг в инструкцию");
        } else {
            steps.forEach(s -> {
                if (isBlank(s.getDescription())) {
                    errors.put("steps", "Заполните описание шага");
                }
            });
        }

        if (isNotEmpty(errors)) throw InvalidRecipeDtoRequestException.builder()
                .errors(errors)
                .build();
    }
}
