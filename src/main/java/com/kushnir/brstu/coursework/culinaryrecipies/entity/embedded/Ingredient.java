package com.kushnir.brstu.coursework.culinaryrecipies.entity.embedded;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class Ingredient {
    private String name;
    private BigDecimal count;
    private String unit;
}
