package com.kushnir.brstu.coursework.culinaryrecipies.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RecipeDto {
    private Long id;
    private String name;
    private String description;
    private short cookingTimeMin;
    private short numberOfServings;
    private Integer dishId;
}
