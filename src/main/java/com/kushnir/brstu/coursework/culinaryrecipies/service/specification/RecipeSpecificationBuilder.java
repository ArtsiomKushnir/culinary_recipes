package com.kushnir.brstu.coursework.culinaryrecipies.service.specification;

import com.kushnir.brstu.coursework.culinaryrecipies.entity.Recipe;
import lombok.RequiredArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isNotBlank;

@RequiredArgsConstructor
public class RecipeSpecificationBuilder {

    private final List<Predicate> predicates;
    private final Root<Recipe> root;
    private final CriteriaBuilder cb;
    private final CriteriaQuery query;

    public static RecipeSpecificationBuilder aPredicates(final Root<Recipe> root, final CriteriaBuilder cb, final CriteriaQuery query) {
        return new RecipeSpecificationBuilder(new ArrayList<>(), root, cb, query);
    }

    public RecipeSpecificationBuilder withName(final String name) {
        if (isNotBlank(name)) {
            predicates.add(cb.like(cb.lower(root.get("name")), "%" + name.toLowerCase() + "%"));
        }
        return this;
    }

    public RecipeSpecificationBuilder withDishId(final Integer dishId) {
        if (dishId != null && dishId > 0) {
            predicates.add(cb.equal(root.get("dish").get("id"), dishId));
        }
        return this;
    }

    public RecipeSpecificationBuilder withCategoryId(final Integer categoryId) {
        if (categoryId != null && categoryId > 0) {
            predicates.add(cb.equal(root.get("dish").get("category").get("id"), categoryId));
        }
        return this;
    }

    public List<Predicate> build() {
        return predicates;
    }
}
