package com.kushnir.brstu.coursework.culinaryrecipies.repository;

import com.kushnir.brstu.coursework.culinaryrecipies.entity.Dish;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DishRepository extends JpaRepository<Dish, Integer> {
}
