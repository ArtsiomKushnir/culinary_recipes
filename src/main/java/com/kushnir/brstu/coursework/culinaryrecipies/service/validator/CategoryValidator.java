package com.kushnir.brstu.coursework.culinaryrecipies.service.validator;

import com.kushnir.brstu.coursework.culinaryrecipies.entity.Category;
import com.kushnir.brstu.coursework.culinaryrecipies.exception.InvalidCategoryRequestException;
import com.kushnir.brstu.coursework.culinaryrecipies.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static org.apache.logging.log4j.util.Strings.isBlank;
import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;

@Component
@RequiredArgsConstructor
public class CategoryValidator {

    private final CategoryRepository categoryRepository;

    public void validate(final Category category) throws InvalidCategoryRequestException {
        final Map<String, String> errors = new HashMap<>();

        if (isBlank(category.getName())) {
            errors.put("name", "Введите название категории");
        } else if (category.getName().length() > 150) {
            errors.put("name", "Превышено ограничение в 150 символов");
        } else if (category.getId() == null && categoryRepository.existsByName(category.getName())) {
            errors.put("name", "Такая категория уже существует");
        }
        
        if (isNotEmpty(category.getDishes())) {
            category.getDishes().forEach(d -> {
                if (isBlank(d.getName())) {
                    errors.put("dishes", "Укажите вид блюда");
                }
            });
        }

        if (isNotEmpty(errors)) throw InvalidCategoryRequestException.builder()
                .errors(errors)
                .build();
    }
}
