package com.kushnir.brstu.coursework.culinaryrecipies.rest;

import com.kushnir.brstu.coursework.culinaryrecipies.entity.Dish;
import com.kushnir.brstu.coursework.culinaryrecipies.repository.DishRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/dishes")
@RequiredArgsConstructor
public class DishController {

    private final DishRepository dishRepository;

    @GetMapping
    public List<Dish> getAll() {
        return dishRepository.findAll();
    }
}
