package com.kushnir.brstu.coursework.culinaryrecipies.exception;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class InvalidRecipeDtoRequestException extends Exception implements BaseException {
    private Map<String, String> errors;
}
