package com.kushnir.brstu.coursework.culinaryrecipies.repository;

import com.kushnir.brstu.coursework.culinaryrecipies.entity.Recipe;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RecipeRepository extends PagingAndSortingRepository<Recipe, Long>, JpaSpecificationExecutor<Recipe> {
}