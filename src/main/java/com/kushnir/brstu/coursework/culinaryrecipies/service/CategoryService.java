package com.kushnir.brstu.coursework.culinaryrecipies.service;

import com.kushnir.brstu.coursework.culinaryrecipies.entity.Category;
import com.kushnir.brstu.coursework.culinaryrecipies.exception.InvalidCategoryRequestException;
import com.kushnir.brstu.coursework.culinaryrecipies.repository.CategoryRepository;
import com.kushnir.brstu.coursework.culinaryrecipies.service.validator.CategoryValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final CategoryValidator categoryValidator;

    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    public Page<Category> getAllCategoriesPageable() {
        return categoryRepository.findAll(Pageable.unpaged());
    }

    public Category create(final Category category) throws InvalidCategoryRequestException {
        categoryValidator.validate(category);
        return categoryRepository.save(category);
    }

    public Category update(final Category category) throws InvalidCategoryRequestException {
        categoryValidator.validate(category);
        return categoryRepository.save(category);
    }

    public Optional<Category> getById(final Integer id) {
        return categoryRepository.findById(id);
    }
}
