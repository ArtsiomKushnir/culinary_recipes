package com.kushnir.brstu.coursework.culinaryrecipies.rest;

import com.kushnir.brstu.coursework.culinaryrecipies.exception.BaseException;
import com.kushnir.brstu.coursework.culinaryrecipies.exception.InvalidCategoryRequestException;
import com.kushnir.brstu.coursework.culinaryrecipies.exception.InvalidRecipeDtoRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@ControllerAdvice
@RestController
public class ExceptionController {
    @ExceptionHandler({InvalidRecipeDtoRequestException.class, InvalidCategoryRequestException.class})
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public Map<String, String> handleInvalidRecipeDtoRequestException(final BaseException e) {
        return e.getErrors();
    }
}
