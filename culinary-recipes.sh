#!/bin/sh

export JAVA_HOME=/home/akushnir/env/java/jdk-11/
cd /home/akushnir/PROJECTS/culinary_recipes;
bash mvnw clean spring-boot:run -Dserver.port=8082 -Drun.jvmArguments='-Dserver.port=8082'
